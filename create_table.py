import sqlite3

connection = sqlite3.connect('data_tasks.db')

cursor = connection.cursor()

create_table = "CREATE TABLE IF NOT EXISTS tasks (id INTEGER PRIMARY KEY, partner_name text, queue_name text, email text, sms text, crontab text)"
cursor.execute(create_table)

connection.commit()

connection.close()
