import sqlite3
import json
from flask_restful import Resource, reqparse


class Task():
    TABLE_NAME = 'tasks'

    def __init__(self, _id, partner_name, queue_name, email, sms, crontab):
        self.id = _id
        self.partner_name = partner_name
        self.queue_name = queue_name
        self.email = email
        self.sms = sms
        self.crontab = crontab

    @classmethod
    def find_by_partner_name(cls, username):
        connection = sqlite3.connect('data_tasks.db')
        cursor = connection.cursor()

        query = "SELECT * FROM {table} WHERE partner_name=?".format(table=cls.TABLE_NAME)
        result = cursor.execute(query, (username,))
        row = result.fetchone()
        if row:
            user = cls(*row)
        else:
            user = None

        connection.close()
        return user

    @classmethod
    def find_by_id(cls, _id):
        connection = sqlite3.connect('data_tasks.db')
        cursor = connection.cursor()

        query = "SELECT * FROM {table} WHERE id=?".format(table=cls.TABLE_NAME)
        result = cursor.execute(query, (_id,))
        row = result.fetchone()
        if row:
            user = cls(*row)
        else:
            user = None

        connection.close()
        return user


class TaskRegister(Resource):
    TABLE_NAME = 'tasks'

    parser = reqparse.RequestParser()
    parser.add_argument('partnerName',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!"
                        )
    parser.add_argument('queueName',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!"
                        )
    parser.add_argument('email',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!"
                        )
    parser.add_argument('sms',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!"
                        )
    parser.add_argument('crontab',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!"
                        )

    def post(self):
        data = TaskRegister.parser.parse_args()

        if Task.find_by_partner_name(data['partnerName']):
            return {"message": "Task with that partnerName already exists."}, 400

        connection = sqlite3.connect('data_tasks.db')
        cursor = connection.cursor()

        query = "INSERT INTO {table} VALUES (NULL, ?, ?, ?, ?, ?)".format(table=self.TABLE_NAME)
        cursor.execute(query, (data['partnerName'], data['queueName'], data['email'], data['sms'], data['crontab']))

        connection.commit()
        connection.close()

        return {"message": "Task created successfully."}, 201

    def get(self):
        connection = sqlite3.connect('data_tasks.db')
        cursor = connection.cursor()

        query = "SELECT * FROM {table}".format(table=self.TABLE_NAME)
        result = cursor.execute(query)

        items = []
        for row in result:
            items.append(
                {
                    'id': row[0],
                    'partnerName': row[1],
                    'queueName': row[2],
                    'email': row[3],
                    'sms': row[4],
                    'crontab': row[5]
                }
            )
        connection.close()

        return {
                   "message": "Get successful task.",
                   "items": items
               }
