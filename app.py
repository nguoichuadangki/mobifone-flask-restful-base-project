from flask import Flask
from flask_restful import Api
from flask_cors import CORS

from task import TaskRegister

app = Flask(__name__)

# Config CORS
CORS(app)

app.config['PROPAGATE_EXCEPTIONS'] = True
app.secret_key = 'jose'
api = Api(app)

api.add_resource(TaskRegister, '/task')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=9001, debug=True)